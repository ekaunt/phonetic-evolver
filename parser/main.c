#define PE_RELEASE
#include <stdio.h>
#include <stdlib.h>
#include "include/print-ast.h"
#include "include/parse-ast.h"
#include "include/parser.tab.h"
#include "include/ast.h"

#define RED     "\x1b[31m"
#define GREEN   "\x1b[32m"
#define YELLOW  "\x1b[33m"
#define BLUE    "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN    "\x1b[36m"
#define RESET   "\x1b[0m"

typedef struct yy_buffer_state * YY_BUFFER_STATE;
extern YY_BUFFER_STATE yy_scan_string(const char * str);

extern EXP* root;


int main(int argc, char** argv) {
	uint8_t tests_passed = 0;

	uint8_t size = 33;

	UChar words[size][50];
	u_uastrcpy(words[0], "alabama");
	u_uastrcpy(words[1], "alabama");
	u_uastrcpy(words[2], "alabama");
	u_uastrcpy(words[3], "alabama");
	u_uastrcpy(words[4], "alabama");
	u_uastrcpy(words[5], "alabama");
	u_uastrcpy(words[6], "alabama");
	u_uastrcpy(words[7], "alabama");
	u_uastrcpy(words[8], "alabama");
	u_uastrcpy(words[9], "alabama");
	u_uastrcpy(words[10], "aalabama");
	u_uastrcpy(words[11], "aalabama");
	u_uastrcpy(words[12], "aalabama");
	u_uastrcpy(words[13], "aalabama");
	u_uastrcpy(words[14], "aalabama");
	u_uastrcpy(words[15], "aalabama");
	u_uastrcpy(words[16], "bbbbbb");
	u_uastrcpy(words[17], "bbbbbb");
	u_uastrcpy(words[18], "bababb");
	u_uastrcpy(words[19], "bababb");
	u_uastrcpy(words[20], "bababb");
	u_uastrcpy(words[21], "bababb");
	u_uastrcpy(words[22], "bacbabb");
	u_uastrcpy(words[23], "nãtʰɑŋ");
	u_uastrcpy(words[24], "alabama");
	u_uastrcpy(words[25], "alabama");
	u_uastrcpy(words[26], "alabama");
	u_uastrcpy(words[27], "alabama");
	u_uastrcpy(words[28], "alabama");
	u_uastrcpy(words[29], "alabama");
	u_uastrcpy(words[30], "alabama");
	u_uastrcpy(words[31], "alabama");
	u_uastrcpy(words[32], "alabama");
	// u_uastrcpy(words[32], "nãtʰɑŋ");

	UChar sound_change_tests[size][50];
	for (uint8_t i = 0; i < size; ++i) {
		u_uastrcpy(sound_change_tests[i], "");
	}
	char* comment[size][50];
	for (uint8_t i = 0; i < size; ++i) {
		strcpy(comment[i], "");
	}
	u_uastrcpy(sound_change_tests[0], "al > e");
	u_uastrcpy(sound_change_tests[1], "a > e");
	u_uastrcpy(sound_change_tests[2], "a > ex");
	u_uastrcpy(sound_change_tests[3], "b > cde");
	u_uastrcpy(sound_change_tests[4], "{b,l} > e"); strcpy(comment[4], "alternates (either)");
	u_uastrcpy(sound_change_tests[5], "{a,b}m > z");
	u_uastrcpy(sound_change_tests[6], "(a) > z"); strcpy(comment[6], "optionals");
	u_uastrcpy(sound_change_tests[7], "a(m) > z");
	u_uastrcpy(sound_change_tests[8], "a({m,b}) > z"); strcpy(comment[8], "alternate + either");
	u_uastrcpy(sound_change_tests[9], "bX > z"); strcpy(comment[9], "any");
	u_uastrcpy(sound_change_tests[10], "a₀ > z"); strcpy(comment[10], "wildcard");
	u_uastrcpy(sound_change_tests[11], "a₀¹ > z");
	u_uastrcpy(sound_change_tests[12], "a₁¹ > z");
	u_uastrcpy(sound_change_tests[13], "a₀a > z"); strcpy(comment[13], "backtracking");
	u_uastrcpy(sound_change_tests[14], "a₁ > z");
	u_uastrcpy(sound_change_tests[15], "a₁a > z");
	u_uastrcpy(sound_change_tests[16], "b₁ > z");
	u_uastrcpy(sound_change_tests[17], "b₁b > z");
	u_uastrcpy(sound_change_tests[18], "{a,b}₁ > z"); strcpy(comment[18], "wildcard + either");
	u_uastrcpy(sound_change_tests[19], "{a,b}₀b > z"); strcpy(comment[19], "wildcard + either + backtracking");
	u_uastrcpy(sound_change_tests[20], "{b₀,a₀} > z");
	u_uastrcpy(sound_change_tests[21], "{a₀,b₀} > z");
	u_uastrcpy(sound_change_tests[22], "{a,b₂,c} > z"); strcpy(comment[22], "wildcard > 1");
	u_uastrcpy(sound_change_tests[23], "{ã,ŋ} > z"); strcpy(comment[23], "unicode");
	u_uastrcpy(sound_change_tests[24], "a > z / _ b"); strcpy(comment[24], "when followed clause");
	u_uastrcpy(sound_change_tests[25], "a > z / b __"); strcpy(comment[25], "when preceded clause");
	u_uastrcpy(sound_change_tests[26], "a > z / b ___ m"); strcpy(comment[26], "when surrounded clause");
	u_uastrcpy(sound_change_tests[27], "a>z/b_m"); strcpy(comment[27], "no spaces");
	u_uastrcpy(sound_change_tests[28], "a > z / b _ b");
	// u_uastrcpy(sound_change_tests[29], "a > z / # _"); strcpy(comment[29], "when beg of word");
	// u_uastrcpy(sound_change_tests[30], "a > z / _ #"); strcpy(comment[30], "when end of word");
	// u_uastrcpy(sound_change_tests[31], "a > z / b _ #");
	// u_uastrcpy(sound_change_tests[32], "a > z / m _ #");
	// u_uastrcpy(sound_change_tests[33], "ã1ɑ2 > 2 1"); strcpy(comment[33], "grouping");

	UChar tests_results[size][50];
	u_uastrcpy(tests_results[0], "eabama");
	u_uastrcpy(tests_results[1], "elebeme");
	u_uastrcpy(tests_results[2], "exlexbexmex");
	u_uastrcpy(tests_results[3], "alacdeama");
	u_uastrcpy(tests_results[4], "aeaeama");
	u_uastrcpy(tests_results[5], "alabza");
	u_uastrcpy(tests_results[6], "zzlzzbzzmz");
	u_uastrcpy(tests_results[7], "zlzbzz");
	u_uastrcpy(tests_results[8], "zlzzz");
	u_uastrcpy(tests_results[9], "alazma");
	u_uastrcpy(tests_results[10], "zzlzzbzzmz");
	u_uastrcpy(tests_results[11], "zzzlzzbzzmz");
	u_uastrcpy(tests_results[12], "zzlzbzmz");
	u_uastrcpy(tests_results[13], "zlzbzmz");
	u_uastrcpy(tests_results[14], "zlzbzmz");
	u_uastrcpy(tests_results[15], "zlabama");
	u_uastrcpy(tests_results[16], "z");
	u_uastrcpy(tests_results[17], "z");
	u_uastrcpy(tests_results[18], "zzzzz");
	u_uastrcpy(tests_results[19], "zzzz");
	u_uastrcpy(tests_results[20], "zzzzz");
	u_uastrcpy(tests_results[21], "zzzzz");
	u_uastrcpy(tests_results[22], "bzzbzz");
	u_uastrcpy(tests_results[23], "nztʰɑz");
	u_uastrcpy(tests_results[24], "alzbama");
	u_uastrcpy(tests_results[25], "alabzma");
	u_uastrcpy(tests_results[26], "alabzma");
	u_uastrcpy(tests_results[27], "alabzma");
	u_uastrcpy(tests_results[28], "alabama");
	u_uastrcpy(tests_results[29], "zlabama");
	u_uastrcpy(tests_results[30], "alabamz");
	u_uastrcpy(tests_results[31], "alabama");
	u_uastrcpy(tests_results[32], "alabamz");
	// u_uastrcpy(tests_results[32], "nɑtʰãŋ");

	#ifdef YYDEBUG
	// yydebug = 1;
	#endif
	for (uint8_t i = 0; i < size; ++i) {
		if (*sound_change_tests[i] == '\0')
			continue;
		const char ascii[50];
		u_austrcpy(ascii, sound_change_tests[i]);
		yy_scan_string(ascii);
		if (!yyparse()) {
			#ifndef PE_RELEASE
			u_printf("Test #%i: %S -> %S = %S\n", i, words[i], sound_change_tests[i], tests_results[i]);
			printEXP(root);
			u_printf("parsed\n");
			#endif
			UChar* result = eval(words[i], root);
			if (u_strcmp(result, tests_results[i]) == 0) {
				u_printf(GREEN"%S %s %S = %S (%s)"RESET"\n", words[i], "→", sound_change_tests[i], result, comment[i]);
				++tests_passed;
			} else {
				u_printf(RED"%S %s %S = %S, %s %S (%s)"RESET"\n", words[i], "→", sound_change_tests[i], tests_results[i], "≠", result, comment[i]);
			}
			EXP_destroy(root);
			free(result);
		} else {
			printf(YELLOW"Parse failed"RESET"\n");
			exit(EXIT_FAILURE);
		}
	}
	u_printf("Tests Passed: %i/%i\n", tests_passed, size);
}
