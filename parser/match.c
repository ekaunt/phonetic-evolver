#include <stdlib.h>
#include <unicode/uchar.h>
#include "include/match.h"

// void match_add(Match** head, Match** tail, UChar str[], int pos) {
// 	Match* new_match = NEW(Match);
// 	int str_len = u_strlen(str);
// 	/*u_printf("tail: %S, %i, %i\n", (*tail)->str, (*tail)->pos, (*tail)->len);*/

// 	new_match->str = (UChar*)malloc(sizeof(UChar)*str_len);
// 	u_strcpy(new_match->str, str);
// 	new_match->pos = pos;
// 	new_match->len = str_len;
// 	new_match->next = NULL;

// 	if (*head == NULL) {
// 		*head = new_match;
// 		*tail = new_match;
// 	} else {
// 		// add the new match to the end of the list
// 		(*tail)->next = new_match;
// 		*tail = (*tail)->next;
// 		u_printf("tail: %S, %i, %i\n", (*tail)->str, (*tail)->pos, (*tail)->len);
// 	}
// }

// void match_destroy(Match* head) {
// 	u_printf("destroying\n");
// 	Match* temp;
// 	for (Match* current = head; current != NULL; current = temp) {
// 		if (current->next != NULL)
// 			temp = current->next;
// 		if (current->str != NULL)
// 			free(current->str);
// 		free(current);
// 	}
// }

void substr(UChar string[], UChar new_string[], int from, int length) {
	int i = 0;
	for (; i < length; ++i)
		new_string[i] = string[i+from];
	new_string[i] = '\0';
}

bool starts_with(UChar* str, UChar* substr) {
	unsigned short substr_len = u_strlen(substr);
	if (u_strlen(str) < substr_len)
		return false;
	// u_printf("%S, %S, %i\n", str, substr, substr_len);
	for (unsigned short i = 0; i < substr_len; ++i) {
		if (str[i] != substr[i]) {
			return false;
		}
	}
	return true;
}

void replace_at_pos(UChar dst[], UChar text[], short pos, uint8_t len, short* total_offset) {
	unsigned short txt_len = u_strlen(text);
	unsigned short dst_len = u_strlen(dst);
	// first shift the characters after whats to be replaced the required amount of characters
	short offset = txt_len-len;
	 /*u_printf("%i, %i, %i, %i\n", pos, len, offset, *total_offset);*/
	if (offset < 0) {
		for (int i = pos-offset; i <= dst_len; ++i) {
			dst[i+offset] = dst[i];
			 /*u_printf("< %C, %C\n", dst[i+offset], dst[i]);*/
		}
	} else if (offset > 0) {
		for (int i = dst_len; i >= pos+*total_offset; --i) {
			dst[i+offset] = dst[i];
			 /*u_printf("> %C\n", dst[i+offset]);*/
		}
	}
	// then do the replacement
	for (int i = 0; i < txt_len; ++i) {
		dst[pos+*total_offset+i] = text[i];
		 /*u_printf("%i, %i, %S, %S, %C, %C\n", i, pos, dst, text, dst[pos+*total_offset+i], text[i]);*/
	}
	*total_offset += offset;
}

void insert_at_pos(UChar dst[], UChar text[], int pos) {
	int len = u_strlen(text);
	// first move up or down all letters after the text to be inserted
	for (int i = u_strlen(dst); i > pos; --i) {
		dst[i+len] = dst[i];
	}
	// we can now safely insert the text into dst
	for (int i = 0; i < len; ++i) {
		dst[pos+i] = text[i];
	}
}

UChar* replace(UChar* src, UChar* what, UChar* with) {
	// calculate the length of the replacement string
	int num_of_what_in_src = 0;
	int srclen = u_strlen(src);
	for (int i = 0; i < srclen; ++i) {
		if (*(src+i) == *(what+0)) {
			int j = 0;
			while (*(what+j)!='\0' && *(src+i+j) == *(what+j))
				++j;
			if (j == u_strlen(what))
				++num_of_what_in_src;
			i += j-1;
		}
	}
	int length = u_strlen(src) - (num_of_what_in_src*u_strlen(what)) + (num_of_what_in_src*u_strlen(with));
	UChar* dst = (UChar*)malloc(sizeof(UChar) * length);

	// position we're up to in dst
	int dst_advance = 0;
	// do the replacement
	for (int src_advance = 0; src_advance < srclen; ) {
		// if the current letter matches
		if (*(src+src_advance) == *(what+0)) {
			int j = 0;
			// then check if its a complete match
			while (*(what+j)!='\0' && *(src+src_advance+j) == *(what+j))
				++j;
			// if it is, do the replacement
			if (j == u_strlen(what)) {
				for (int k = 0; *(with+k) != '\0'; ++k) {
					*(dst+dst_advance+k) = *(with+k);
				}
				dst_advance += j-1;
			// if not, copy the characters
			} else {
				*(dst+dst_advance) = *(src+src_advance);
				++dst_advance;
			}
			src_advance += j;
		// if not, copy the characters verbatim
		} else {
			*(dst+dst_advance) = *(src+src_advance);
			++dst_advance;
			++src_advance;
		}
	}

	return dst;
}

void match_init(Matches** matches) {
	*matches = NEW(Matches);
	(*matches)->pos = 0;
	(*matches)->allocated = 8;
	(*matches)->matches = (Match**)malloc(sizeof (Match*) * (*matches)->allocated);
}

static void match_realloc(Matches* matches) {
	if (matches->pos == matches->allocated-1) {
		matches->allocated *= 2;
		matches->matches = realloc(matches->matches, (sizeof *matches->matches) * matches->allocated);
	}
}

void match_add(Matches* matches, Match* match) {
	// u_printf("%i, %i\n", matches->pos, matches->allocated);
	match_realloc(matches);
	matches->matches[matches->pos] = match;
	++matches->pos;
	// free(match);
}

void match_destroy(Matches** matches) {
	for (uint8_t i = 0; i < (*matches)->pos; ++i) {
		free((*matches)->matches[(*matches)->pos]);
	}
	free(*matches);
}