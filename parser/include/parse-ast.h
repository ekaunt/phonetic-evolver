#define PE_RELEASE
#ifndef PE_EVAL
#define PE_EVAL

#include <stdlib.h>
#include "ast.h"
#include "traverser.h"

#define SAYI(var) 			u_printf("%i: "#var": %i\n",__LINE__, var)
#define SAYS(var) 			u_printf("%i: "#var": %i\n",__LINE__, var)

/**
 * @brief a sound change type
 */
typedef enum
{
	RuleRegular,
	RuleMetathesis,
	RuleCapture
} RuleKind;

// typedef struct Traverser Traverser;

/**
 * go through a parse tree and apply sound changes
 * 
 * word: the string to match against
 * exp: the parse tree
 * 
 * returns: the string with the sound changes applied
 */
UChar* eval(UChar* word, EXP* exp);

/**
 * @brief traverse the lhs of a phonological rule
 * 
 * @param traverser the traverser to evaluate
 * @param word the string to match against
 * @param outer_kind the ExpressionKind that we're inside of
 * @param current_match what was already matched so far
 * @param pos the position in `word` that we're up to
 * 
 * @returns match length
 */
short eval(Traverser* lhs, UChar* word, ExpressionKind outer_kind, UChar current_match[], uint8_t* pos);

/**
 * traverse the rhs of a phonological rule
 */
void eval_rhs(Traverser* rhs);

/**
 * move the cursor along in the stack till we get to the end of the current recursive "indent"
 */
// void eval_skip_to_end(Traverser*);

void eval_statement(EXP*, EXP*);
void eval_statement_when(EXP*, EXP*, EXP*);

void eval_lhs_section(EXP*);
void eval_lhs_either(EXP*, EXP*);
void eval_lhs_literal(EXP*);
void eval_lhs_quantifier(EXP*);
void eval_lhs_capture(EXP*);

void eval_rhs_section(EXP*);
void eval_rhs_quantifier(EXP*);
void eval_rhs_capture(EXP*);
void eval_rhs_literal(EXP*);

void eval_whs_section(EXP*, Traverser*);
void eval_whs_either(EXP*, EXP*, Traverser*);
void eval_whs_quantifier(EXP*, Traverser*);
void eval_whs_capture(EXP*, Traverser*);
void eval_whs_literal(EXP*, Traverser*);
// void match_literal(UChar[]);
// void replace_literal(UChar[], UChar[]);

// UChar* evalEXP(EXP*);

#endif
