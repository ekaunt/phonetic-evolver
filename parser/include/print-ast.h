#ifndef PE_PRINT_AST
#define PE_PRINT_AST

#include "ast.h"

void print_indents(int);
void print_indented(EXP*, int);
void printEXP(EXP* e);

#endif