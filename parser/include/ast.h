#ifndef PE_AST
#define PE_AST
#include <unicode/uchar.h>

#define NEW(type) (type*)malloc(sizeof(type))

typedef enum {
	KindEof,
	KindEnd,
	KindAny,
	KindLiteral,
	KindOptional,
	KindEither,
	KindSection, 
	KindBinary,
	KindQuantifier,
	KindCapture,
	KindCaptureNumber,
	KindSubscript,
	KindSuperscript,
	KindStatement,
	KindStatementWhen,
	KindWHS,
} ExpressionKind;

typedef struct EXP EXP;
struct EXP {
	int lineno;
	ExpressionKind kind;
	union {
		// literal
		UChar str[64];
		// unary
		struct {
			union {
				EXP* section;
				// binary / either
				struct {
					EXP* left;
					EXP* right;
				};
			};
			EXP* postposition;
			EXP* capture;
		};
	};
};

EXP* EXP_make_section(EXP*, EXP*, EXP*);
EXP* EXP_make_statement(EXP*, EXP*);
EXP* EXP_make_statement_when(EXP*, EXP*, EXP*);
EXP* EXP_make_whs(EXP*, EXP*);
EXP* EXP_make_binary(EXP*, EXP*, EXP*, EXP*);
EXP* EXP_make_trinary(EXP*, EXP*, EXP*);
EXP* EXP_make_literal(char*);
EXP* EXP_make_quantifier(char*, char*);
EXP* EXP_make_capture(char*);
EXP* EXP_make_optional(EXP*);
EXP* EXP_make_any();
EXP* EXP_make_either(EXP*, EXP*);
void EXP_destroy(EXP* root);
#endif
