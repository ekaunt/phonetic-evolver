#ifndef PE_CAPTURES
#define PE_CAPTURES

#include <stdlib.h>
#include <unicode/uchar.h>

// the total size of the captures array
#define CAPTURES_SIZE 32
// max size of a capture
#define CAPTURES_MAX_SIZE 64

extern UChar captures[CAPTURES_SIZE][CAPTURES_MAX_SIZE];

/**
 * add a capture
 */
void capture_set(uint8_t pos, UChar capture[]);

/**
 * retrieve a capture
 */
UChar* capture_get(uint8_t pos);

#endif