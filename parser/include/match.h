#ifndef PE_MATCH
#define PE_MATCH

#include <stdlib.h>
#include <unicode/uchar.h>
#include "ast.h"


// insert `text` into `dst` at a given `pos`ition
void insert_at_pos(UChar dst[], UChar text[], int pos);
// replace in `dst` whats between `pos` and `len` with `text`
void replace_at_pos(UChar dst[], UChar text[], short pos, uint8_t len, short* offset);
// do a global search and replace 
// void replace_global(UChar* dst, UChar* what, UChar* with);
UChar* replace(UChar* src, UChar* what, UChar* with);
// check if a given `str` begins with `substr`
bool starts_with(UChar* str, UChar* substr);
// gets the substring of a given string
void substr(UChar string[], UChar new_string[], int from, int length);


/****************
***** MATCH *****
****************/

typedef struct {
	// the position in the word that this match starts
	uint8_t position;
	// the amount of characters in the source word that this match will replace
	uint8_t matched_length;
} Match;

typedef struct {
	// our position in the array
	uint8_t pos;
	// amount of space allocated for the list of matches
	uint8_t allocated;
	// the list of matches (resizable)
	Match** matches;
} Matches;

// initialize a Matches struct
void match_init(Matches** matches);
// increase the amount of matches that matches can hold
static void match_realloc(Matches* matches);
// add a match to the matches
void match_add(Matches* matches, Match* match);
// frees the entire Matches struct
void match_destroy(Matches** matches);

#endif
