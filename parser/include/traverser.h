#ifndef PE_TRAVERSER
#define PE_TRAVERSER

#include <stdlib.h>
#include <unicode/uchar.h>
#include "ast.h"

typedef struct {
	UChar* str;
	ExpressionKind kind;
} Pair;

Pair* pair_new(UChar* str, ExpressionKind kind);

/********************************
 * simple interface for a traverser *
 *******************************/

/**
 * represents a node in the traverser
 */
typedef struct Node Node;
struct Node {
	Pair* pair;
	Node* next;
};

/**
 * a data structure like a queue, except the elements dont get popped off the queue when reading from it, 
 * therefore you can always restart from the beginning of the Traverser
*/
typedef struct {
	// the number of elements in the list
	uint8_t count;
	// the position in the list we're up to
	Node* current;
	// first element in the list
	Node* head;
	// last element in the list
	Node* tail;
} Traverser;

/**
 * create a new traverser
 */
void traverser_new(Traverser**);

/**
 * destroy and free a traverser
 */
void traverser_destroy(Traverser**);

/**
 * set the traversers cursor back to the beginning or to the Node specified
 */
void traverser_restart(Traverser*, Node* from);

/**
 * check if the traverser has any nodes
 */
uint8_t traverser_is_empty(Traverser*);

/**
 * add a node to the traverser
 */
void traverser_append(Traverser*, Pair*);

/**
 * move the traversers cursor one node down, and return that node
 */
Node* traverser_next_node(Traverser*);

/**
 * move the traversers cursor one node down, and return the data associated with that node
 */
Pair* traverser_next(Traverser*);

/**
 * check the next node in the list without moving the cursor
 */
Pair* traverser_peek(Traverser*);

#endif
