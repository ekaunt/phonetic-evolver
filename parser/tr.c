#include <string.h>
#include <unicode/uchar.h>
#include "include/tr.h"

char* tr(char str[], char from[], char to[]) {
	UChar ustr[50];
	UChar ufrom[50];
	UChar uto[50];
	u_uastrcpy(ustr, str);
	u_uastrcpy(ufrom, from);
	u_uastrcpy(uto, to);

	/*u_printf("%S\n", ustr);*/
	uint8_t str_len = u_strlen(ustr);
	uint8_t from_len = u_strlen(ufrom);
	for (uint8_t i = 0; i < str_len; ++i) {
		for (uint8_t j = 0; j < from_len; ++j) {
			if (ustr[i] == ufrom[j]) {
				ustr[i] = uto[j];
				break;
			}
		}
	}
	u_austrcpy(str, ustr);
	/*u_printf("%s\n", str);*/
	return str;
}
