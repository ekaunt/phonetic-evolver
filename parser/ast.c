#include <stdlib.h>
#include "include/ast.h"

extern int yylineno;

EXP* EXP_make_statement(EXP* lhs, EXP* rhs) {
	EXP* e = malloc(sizeof(EXP));

	e->lineno = yylineno;
	e->kind = KindStatement;
	e->left = lhs;
	e->right = rhs;

	return e;
}

EXP* EXP_make_statement_when(EXP* lhs, EXP* rhs, EXP* whs) {
	EXP* e = malloc(sizeof(EXP));

	e->lineno = yylineno;
	e->kind = KindStatementWhen;
	e->left = lhs;
	e->right = rhs;
	e->postposition = whs;

	return e;
}

EXP* EXP_make_whs(EXP* left, EXP* right) {
	EXP* e = malloc(sizeof(EXP));

	e->lineno = yylineno;
	e->kind = KindWHS;
	
	e->left = left;
	if (e->left) {
		e->left->postposition = NULL;
		e->left->capture = NULL;
	}

	e->right = right;
	if (e->right) {
		e->right->postposition = NULL;
		e->right->capture = NULL;
	}

	return e;
}

EXP* EXP_make_section(EXP* section, EXP* postposition, EXP* capture) {
	EXP* e = malloc(sizeof(EXP));

	e->lineno = yylineno;
	e->kind = KindSection;
	e->section = section;
	e->postposition = postposition;
	e->capture = capture;

	return e;
}


EXP* EXP_make_binary(EXP* left, EXP* right, EXP* postposition, EXP* capture) {
	EXP* e = malloc(sizeof(EXP));

	e->lineno = yylineno;
	e->kind = KindBinary;
	e->left = left;
	e->right = right;
	e->postposition = postposition;
	e->capture = capture;

	return e;
}

EXP* EXP_make_any() {
	EXP* e = malloc(sizeof(EXP));

	e->lineno = yylineno;
	e->kind = KindAny;

	return e;
}


EXP* EXP_make_quantifier(char* subscript, char* superscript) {
	EXP* e = malloc(sizeof(EXP));
	EXP* left = malloc(sizeof(EXP));
	EXP* right = malloc(sizeof(EXP));

	e->lineno = yylineno;
	e->kind = KindQuantifier;

	e->left = left;
	e->left->lineno = yylineno;
	e->left->kind = KindSubscript;
	u_uastrcpy(e->left->str, subscript);
	e->left->postposition = NULL;
	e->left->capture = NULL;

	e->right = right;
	e->right->lineno = yylineno;
	e->right->kind = KindSuperscript;
	if (superscript != NULL)
		u_uastrcpy(e->right->str, superscript);
	else
		u_uastrcpy(e->right->str, "");
	e->right->postposition = NULL;
	e->right->capture = NULL;

	return e;
}

EXP* EXP_make_literal(char* string) {
	EXP* e = malloc(sizeof(EXP));

	e->lineno = yylineno;
	e->kind = KindLiteral;
	u_uastrcpy(e->str, string);

	return e;
}

EXP* EXP_make_capture(char* number) {
	EXP* e = malloc(sizeof(EXP));

	e->lineno = yylineno;
	e->kind = KindCapture;
	u_uastrcpy(e->str, number);

	return e;
}

EXP* EXP_make_optional(EXP* exp) {
	EXP* e = malloc(sizeof(EXP));

	e->lineno = yylineno;
	e->kind = KindOptional;
	e->section = exp;

	return e;
}

EXP* EXP_make_either(EXP* exp1, EXP* exp2) {
	EXP* e = malloc(sizeof(EXP));

	e->lineno = yylineno;
	e->kind = KindEither;
	e->left = exp1;
	e->right = exp2;
	e->left->postposition = NULL;
	e->left->capture = NULL;
	e->postposition = NULL;
	e->capture = NULL;

	return e;
}

void EXP_destroy(EXP* root) {
	switch (root->kind) {
	case KindSection:
		EXP_destroy(root->section);
		break;

	case KindBinary:
		EXP_destroy(root->left);
		EXP_destroy(root->right);
		break;

	case KindLiteral:
		free(root);
		break;

	case KindOptional:
		EXP_destroy(root->section);
		break;

	case KindEither:
		EXP_destroy(root->left);
		EXP_destroy(root->right);
		break;

	case KindStatement:
		EXP_destroy(root->left);
		EXP_destroy(root->right);
		break;

	case KindStatementWhen:
		EXP_destroy(root->left);
		EXP_destroy(root->right);
		break;

	case KindAny:
		free(root);
		break;
	
	case KindQuantifier:
		EXP_destroy(root->left);
		if (root->right->str[0] != '\0')
			EXP_destroy(root->right);
		break;

	case KindSubscript:
		free(root);
		break;

	case KindSuperscript:
		free(root);
		break;
	case KindCapture:
		free(root);
		break;
	}

	if (root->postposition != NULL) {
		EXP_destroy(root->postposition);
	}
	if (root->capture != NULL)
		EXP_destroy(root->capture);
}
