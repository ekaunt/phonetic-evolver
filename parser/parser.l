%{
#include <unicode/uchar.h>
#include <string.h>
#include "parser.tab.h"
#include "include/tr.h"
%}

%option yylineno warn noyywrap

CAPS					[A-Z]

DIGIT 					[0-9]

SUBSCRIPT 				[₀₁₂₃₄₅₆₇₈₉]
SUPERSCRIPT 			[⁰¹²³⁴⁵⁶⁷⁸⁹]

%%

[,] 					return COMMA;

[>] 					return BECOMES;
"//" 					return SURROUND;
[/] 					return WHEN;
[_]+					return PLACEHOLDER;

[(] 					return LEFTPAREN;
[)] 					return RIGHTPAREN;

[{] 					return LEFTBRACE;
[}] 					return RIGHTBRACE;

[X]						return X;
[∅ε]					return EMPTY;

{CAPS}+					{ yylval.letters = strdup(yytext); return CAPS; }
{DIGIT}+				{ yylval.letters = strdup(yytext); return CAPTURE; }

{SUBSCRIPT}+			{ yylval.letters = tr(strdup(yytext), "₀₁₂₃₄₅₆₇₈₉", "0123456789"); return SUBSCRIPT; }
{SUPERSCRIPT}+			{ yylval.letters = tr(strdup(yytext), "⁰¹²³⁴⁵⁶⁷⁸⁹", "0123456789"); return SUPERSCRIPT; }

[\n\r\f\v]+ 			return EOL;

[ \t]+ /*dont do anything with spaces*/

 /* if we got up to this point then its a miscellaneous symbol
	that just needs to be interpreted as a letter */
[^ \n(){}<>[\],∅>/_A-Z0-9₀₁₂₃₄₅₆₇₈₉⁰¹²³⁴⁵⁶⁷⁸⁹]+	{ yylval.letters = strdup(yytext); return LETTERS; }

%%
