#include <unicode/uchar.h>
#include <stdlib.h>
#include "include/parser.tab.h"
#include "include/parse-ast.h"
#include "include/match.h"
#include "include/captures.h"
#ifdef PE_RELEASE
#undef u_printf
#define u_printf
#endif

// the word with the sound changes applied
UChar* dest;
// a way to keep track of whats going on with the matches
Traverser* lhs_traverser;
// a way to keep track of whats going on with the matches
Traverser* rhs_traverser;
Traverser* whs_preceded_traverser;
Traverser* whs_followed_traverser;
// what to replace the lhs with
// we can have multiple replacements
// 0ᵗʰ pos in replacements[] is reserved for when we dont have any captures set
UChar replacements[CAPTURES_SIZE][CAPTURES_MAX_SIZE];
// the current capture group
uint8_t replacements_pos = 1;
// a change to this variable will change how the parser decides what to do
RuleKind current_rule_kind;


UChar* eval(UChar* word, EXP* exp) {
	UChar* dest = (UChar*)calloc(128, sizeof (UChar));
	current_rule_kind = RuleRegular;
	traverser_new(&lhs_traverser);
	traverser_new(&rhs_traverser);
	// clean all captures
	for (uint8_t i = 0; i < CAPTURES_SIZE; ++i)
		u_uastrcpy(replacements[i], "");
	for (uint8_t i = 0; i < CAPTURES_SIZE; ++i)
		u_uastrcpy(capture_get(i), "");

	// traverse the AST, and generate a linked list of matches
	switch (exp->kind)
	{
	case KindStatement:
		eval_statement(exp->left, exp->right);
		break;
	case KindStatementWhen:
		traverser_new(&whs_preceded_traverser);
		traverser_new(&whs_followed_traverser);
		eval_statement_when(exp->left, exp->right, exp->postposition);
		break;
	}

	// traverse the linked list and do the replacements
	u_strcpy(dest, word);

	short offset = 0;
	// the list of matches (resizable)
	Matches* matches;
	match_init(&matches);
	uint8_t word_len = u_strlen(word);
	for (uint8_t i = 0; i < word_len; ++i) {
		UChar current_match[64];
		u_uastrcpy(current_match, "");
		traverser_restart(lhs_traverser, NULL);
		if (whs_preceded_traverser)
			traverser_restart(whs_preceded_traverser, NULL);
		if (whs_followed_traverser)
			traverser_restart(whs_followed_traverser, NULL);
		SAYI(i);
		short whs_preceded_match_length = 0, whs_followed_match_length = 0;
		// match when preceded by
		if (whs_preceded_traverser && traverser_peek(whs_preceded_traverser)->kind != KindEof) {
			UChar empty[64];
			u_uastrcpy(empty, "");
			whs_preceded_match_length = eval(whs_preceded_traverser, word, KindStatementWhen, empty, &i); 
			// skip to the next char in `word` if the 'lookbehind' didnt match anything
			if (whs_preceded_match_length > 0)
				i += whs_preceded_match_length;
			else
				continue;
		}
		// match the actual lhs
		short match_length = eval(lhs_traverser, word, KindStatement, current_match, &i);
		Match* match = NEW(Match);
		match->position = i;
		if (match_length > 1)
			i += match_length;
		// match when followed by
		if (whs_followed_traverser && traverser_peek(whs_followed_traverser)->kind != KindEof) {
			whs_followed_match_length = eval(whs_followed_traverser, word, KindStatementWhen, current_match, &i);
		}
		// for the ++i in the for loop
		if (match_length > 1)
			--i;
		SAYI(match_length);
		SAYI(whs_preceded_match_length);
		SAYI(whs_followed_match_length);
		// 0 match length means insertion
		if (match_length >= 0 && whs_preceded_match_length >= 0 && whs_followed_match_length >= 0) {
			u_printf("Created match: from %i for %i chars\n", i, match_length);
			match->matched_length = match_length;
			// add the new match to the end of the list
			match_add(matches, match);
		} else
			free(match);
	}

	eval_rhs(rhs_traverser);

	switch (current_rule_kind)
	{
	case RuleRegular:
		for (uint8_t i = 0; i < matches->pos; ++i) {
			u_printf("match: pos: %i, match_len: %i\n", matches->matches[i]->position, matches->matches[i]->matched_length);
			replace_at_pos(dest, replacements[0], matches->matches[i]->position, matches->matches[i]->matched_length, &offset);
		}
		break;
	}
	match_destroy(&matches);
	traverser_destroy(&lhs_traverser);
	traverser_destroy(&rhs_traverser);
	if (whs_preceded_traverser)
		traverser_destroy(&whs_preceded_traverser);
	if (whs_followed_traverser)
		traverser_destroy(&whs_followed_traverser);
	return dest;
}

UChar current_submatch[64];
short eval(Traverser* lhs, UChar* word, ExpressionKind outer_kind, UChar current_match[], uint8_t* pos) {
	UChar temp_match[64];
	u_uastrcpy(current_submatch, "");
	short match_length = 0;
	Pair* current_pair;
	do {
		current_pair = traverser_next(lhs);
		if (current_pair->str != NULL) {
			u_strcpy(temp_match, current_match);
			u_strcat(temp_match, current_pair->str);
			u_strcpy(current_submatch, current_pair->str);
			// u_printf("match: %S, str: %S, kind: %i\n", current_match, current_pair->str, current_pair->kind);
		}
		switch (current_pair->kind) {
		case KindEof:
			return match_length;
		case KindEnd: {
			return match_length;
		}
		case KindLiteral:
			u_printf("literal: %S, match: %S, temp_match: %S\n", current_pair->str, current_match, temp_match);
			if (starts_with((word+*pos), temp_match)) {
				match_length += u_strlen(current_pair->str);
				u_printf("current: %S, match_len: %i\n", temp_match, match_length);
				if (outer_kind != KindEither)
					u_strcat(current_match, current_pair->str);
			} else if (outer_kind != KindEither) {
				// if we need to match a literal, but it didnt get matched, 
				// and if we're not in an either block, then the whole match should fail
				match_length = -1;
			}
			break;
		case KindEither: {
			// the match in the either that matched the most characters
			short max_either = 0;
			while (traverser_peek(lhs)->kind != KindEnd) {
				short either = eval(lhs, word, KindEither, current_match, pos);
				if (either > max_either) {
					max_either = either;
				}
			}
			// skip over the final KindEnd
			current_pair = traverser_next(lhs);
			if (max_either <= 0)
				match_length = -1;
			else {
				// set current_submatch to the max matched thing in the either
				substr(word, current_submatch, *pos+match_length, max_either);
				u_strcat(current_match, current_submatch);
				match_length += max_either;
			}
			u_printf("i: %i, match_length: %i, max_either: %i\n", *pos, match_length, max_either);
			break;
		}
		case KindOptional: {
			short optional = eval(lhs, word, KindOptional, current_match, pos);
			if (optional > 0)
				match_length += optional;
			break;
		}
		case KindQuantifier: {
			// figure out the amount times to repeat the last submatch
			Pair* subscript = traverser_next(lhs);
			char a_subscript[64];
			u_austrcpy(a_subscript, subscript->str);
			uint8_t min_amount = atoi(a_subscript);
			uint8_t max_amount = UINT8_MAX;
			Node* after_backtrack = traverser_next_node(lhs);
			Pair* superscript = after_backtrack->pair;
			if (superscript->str[0] != '\0') {
				char a_superscript[64];
				u_austrcpy(a_superscript, superscript->str);
				max_amount = atoi(a_superscript);
			}

			UChar temp_submatch[64];
			u_uastrcpy(temp_submatch, current_match);
			// insert a character if we didnt match
			if (min_amount == 0 && match_length == -1) {
				match_length = 0;
				break;
			}
			// if the maximum match amount is 1, then we already matched it
			if (max_amount == 1) {
				return match_length;
			}
			// greedy match as much as we can
			int8_t times_matched = 0;
			// if we already matched once before, then reflect it in times_matched
			if (u_strlen(temp_submatch) > 0 && starts_with((word+*pos), temp_submatch)) {
				++times_matched;
			}
			while (true) {
				bool one_too_many = false;
				u_strcat(temp_submatch, current_submatch);
				u_printf("times_matched: %i, will try to match: %S, current_submatch: %S\n", times_matched, temp_submatch, current_submatch);
				if (starts_with((word+*pos), temp_submatch)) {
					u_printf("temp_submatch: %S, match_len: %i\n", temp_submatch, match_length);
					++times_matched;
					one_too_many = true;

					// calculate the match_length
					match_length += u_strlen(current_submatch);
					SAYI(match_length);
				} else {
					SAYI(times_matched);
					break;
				}
				if (!(times_matched < max_amount && times_matched <= u_strlen(word))) {
					if (one_too_many)
						--times_matched;
					SAYI(times_matched);
					break;
				}
			}
			// if we didnt reach the minimum amount, then fail the match
			if (times_matched < min_amount) {
				if (outer_kind == KindEither && traverser_peek(lhs)->kind == KindEnd)
					traverser_next(lhs);
				return match_length = -1;
			}

			// backtrack if we matched too much
			// dont try to backtrack if we reached the end of the rule
			int8_t match_length_before_backtracking = match_length;
			if (traverser_peek(lhs)->kind != KindEof) {
				if (!(times_matched >= min_amount && times_matched >= 0)) {
					match_length -= u_strlen(current_submatch);
					// skip past the KindEnd if we're in a KindEither block
					if (outer_kind == KindEither && traverser_peek(lhs)->kind == KindEnd)
						traverser_next(lhs);
					break;
				}
				for (; times_matched >= min_amount && times_matched >= 0; --times_matched) {
					// make a temporary string to check a match against
					UChar backtrack[64];
					u_uastrcpy(backtrack, "");
					SAYI(times_matched);
					// make backtracking work when the min_amount is 0
					if (min_amount > 0)
						u_strcat(backtrack, current_submatch);
					for (int8_t i = times_matched-1; i > 1; --i) {
						SAYI(i);
						u_strcat(backtrack, current_submatch);
					}
					u_printf("current_submatch: %S, backtrack: %S\n", current_submatch, backtrack);
					match_length -= u_strlen(current_submatch);
					SAYI(match_length);

					// the length of the quantifier after backtracking, and matching the rest of the string
					short backtracked_match_length = eval(lhs, word, outer_kind, backtrack, pos);
					u_printf("backtracked length: %i\n", backtracked_match_length);
					if (backtracked_match_length >= 0) {
						match_length += backtracked_match_length;
						SAYI(backtracked_match_length);
						SAYI(match_length);
						return match_length;
					}
					traverser_restart(lhs, after_backtrack);
				}
			}
			// if we reach here, then the backtracking didnt work, so revert the changes
			match_length = match_length_before_backtracking;
			break;
		}
		case KindAny: {
			if (match_length >= 0)
				match_length += 1;
			break;
		}
		case KindCapture: {
			if (match_length > 0) {
				char capture_str[CAPTURES_MAX_SIZE] = "0";
				current_pair = traverser_next(lhs);
				if (current_pair->str != NULL)
					u_austrcpy(capture_str, current_pair->str);
				uint8_t capture = atoi(capture_str);
				capture_set(capture, current_submatch);
			}
			break;
		}
		}
	} while (current_pair->kind != KindEnd || current_pair->kind != KindEof);
	return match_length;
}

void eval_rhs(Traverser* rhs) {
	Pair* current_pair;
	do {
		current_pair = traverser_next(rhs);
		// if (current_pair->str != NULL) {
			// u_printf("match: %S, str: %S, kind: %i\n", current_match, current_pair->str, current_pair->kind);
		// }
		switch (current_pair->kind)
		{
		case KindEof:
			return;
		case KindLiteral: {
			// u_strncpy(replacements[0], current_pair->str, CAPTURES_MAX_SIZE);
			u_strcpy(replacements, current_pair->str);
			break;
		}
		case KindCapture: {
			char capture_str[CAPTURES_MAX_SIZE] = "0";
			if (current_pair->str != NULL) {
				u_austrcpy(capture_str, current_pair->str);
				uint8_t capture = atoi(capture_str);
				u_strncpy(replacements[replacements_pos++], capture_get(capture), CAPTURES_MAX_SIZE);
			}
			break;
		}
		}
	} while (current_pair->kind != KindEnd || current_pair->kind != KindEof);
}


/* - - - - - - - - - - - - - EVAL STATEMENT - - - - - - - - - - - - - */

void eval_statement_when(EXP* lhs, EXP* rhs, EXP* whs) {
	eval_statement(lhs, rhs);
	if(whs->left)
		eval_whs_section(whs->left, whs_preceded_traverser);
	if(whs->right)
		eval_whs_section(whs->right, whs_followed_traverser);

	Pair* eof = pair_new(NULL, KindEof);
	traverser_append(whs_preceded_traverser, eof);
	Pair* eof2 = pair_new(NULL, KindEof);
	traverser_append(whs_followed_traverser, eof2);
}

void eval_statement(EXP* lhs, EXP* rhs) {
	switch (lhs->kind)
	{
	case KindSection:
		eval_lhs_section(lhs->section);
		break;
	case KindBinary:
		eval_lhs_section(lhs->left);
		eval_lhs_section(lhs->right);
		break;
	case KindLiteral:
		eval_lhs_literal(lhs);
		break;
	}
	if (lhs->postposition != NULL)
		eval_lhs_section(lhs->postposition);
	if (lhs->capture != NULL)
		eval_lhs_section(lhs->capture);
	Pair* end = pair_new(NULL, KindEof);
	traverser_append(lhs_traverser, end);

	switch (rhs->kind)
	{
	case KindSection:
		eval_rhs_section(rhs->section);
		break;
	case KindBinary:
		eval_rhs_section(rhs->left);
		eval_rhs_section(rhs->right);
		break;
	}
	if (rhs->capture != NULL)
		eval_rhs_section(rhs->capture);

	Pair* end2 = pair_new(NULL, KindEof);
	traverser_append(rhs_traverser, end2);
}

/* - - - - - - - - - - - - LHS - - - - - - - - - - - - - */

void eval_lhs_section(EXP* section) {
	switch (section->kind) {
	case KindSection:
		eval_lhs_section(section->section);
		break;
	case KindBinary:
		eval_lhs_section(section->left);
		eval_lhs_section(section->right);
		break;
	case KindEither:
		eval_lhs_either(section->left, section->right);
		break;
	case KindOptional: {
		Pair* optional = pair_new(NULL, KindOptional);
		traverser_append(lhs_traverser, optional);
		eval_lhs_section(section->section);
		Pair* end = pair_new(NULL, KindEnd);
		traverser_append(lhs_traverser, end);
		break;
	}
	case KindAny: {
		Pair* any = pair_new(NULL, KindAny);
		traverser_append(lhs_traverser, any);
		break;
		}
	case KindLiteral: {
		eval_lhs_literal(section);
		break;
	}
	case KindQuantifier: {
		eval_lhs_quantifier(section);
		break;
	}
	case KindCapture: {
		eval_lhs_capture(section);
		break;
	}
	}
	if (section->postposition != NULL)
		eval_lhs_section(section->postposition);
	if (section->capture != NULL)
		eval_lhs_section(section->capture);
}

void eval_lhs_quantifier(EXP* exp) {
	Pair* quanitfier = pair_new(NULL, KindQuantifier);
	Pair* subscript = pair_new(exp->left->str, KindSubscript);
	Pair* superscript = pair_new(exp->right->str, KindSuperscript);
	traverser_append(lhs_traverser, quanitfier);
	traverser_append(lhs_traverser, subscript);
	traverser_append(lhs_traverser, superscript);
}

void eval_lhs_capture(EXP* exp) {
	Pair* capture = pair_new(NULL, KindCapture);
	Pair* capture_number = pair_new(exp->str, KindCaptureNumber);
	traverser_append(lhs_traverser, capture);
	traverser_append(lhs_traverser, capture_number);
}

void eval_lhs_either(EXP* left, EXP* right) {
	Pair* either = pair_new(NULL, KindEither);
	traverser_append(lhs_traverser, either);
	switch (left->kind)
	{
	case KindSection:
		eval_lhs_section(left->section);
		break;
	case KindEither:
		eval_lhs_either(left->left, left->right);
		break;
	}
	if (left->postposition != NULL)
		eval_lhs_section(left->postposition);
	Pair* end = pair_new(NULL, KindEnd);
	traverser_append(lhs_traverser, end);
	switch (right->kind)
	{
	case KindSection:
		eval_lhs_section(right->section);
		break;
	case KindEither:
		eval_lhs_either(right->left, right->right);
		break;
	}
	if (right->postposition != NULL)
		eval_lhs_section(right->postposition);
	Pair* end2 = pair_new(NULL, KindEnd);
	traverser_append(lhs_traverser, end2);
	Pair* end3 = pair_new(NULL, KindEnd);
	traverser_append(lhs_traverser, end3);
}

void eval_lhs_literal(EXP* exp) {
	Pair* literal = pair_new(exp->str, KindLiteral);
	traverser_append(lhs_traverser, literal);
}

/* - - - - - - - - - - - - RHS - - - - - - - - - - - - */

void eval_rhs_section(EXP* section) {
	switch (section->kind) {
	case KindLiteral: {
		eval_rhs_literal(section);
		break;
	}
	case KindCapture: {
		current_rule_kind = RuleCapture;
		Pair* capture = pair_new(section->str, KindCapture);
		traverser_append(rhs_traverser, capture);
		break;
	}
	}
	if (section->capture != NULL)
		eval_rhs_section(section->capture);
}

void eval_rhs_literal(EXP* exp) {
	Pair* literal = pair_new(exp->str, KindLiteral);
	traverser_append(rhs_traverser, literal);
}

/* - - - - - - - - - - - - WHS - - - - - - - - - - - - - */

void eval_whs_section(EXP* section, Traverser* whs_traverser) {
	switch (section->kind) {
	case KindSection:
		eval_whs_section(section->section, whs_traverser);
		break;
	case KindBinary:
		eval_whs_section(section->left, whs_traverser);
		eval_whs_section(section->right, whs_traverser);
		break;
	case KindEither:
		eval_whs_either(section->left, section->right, whs_traverser);
		break;
	case KindOptional: {
		Pair* optional = pair_new(NULL, KindOptional);
		traverser_append(whs_traverser, optional);
		eval_whs_section(section->section, whs_traverser);
		Pair* end = pair_new(NULL, KindEnd);
		traverser_append(whs_traverser, end);
		break;
	}
	case KindAny: {
		Pair* any = pair_new(NULL, KindAny);
		traverser_append(whs_traverser, any);
		break;
		}
	case KindLiteral: {
		eval_whs_literal(section, whs_traverser);
		break;
	}
	case KindQuantifier: {
		eval_whs_quantifier(section, whs_traverser);
		break;
	}
	case KindCapture: {
		eval_whs_capture(section, whs_traverser);
		break;
	}
	}
	if (section->postposition != NULL)
		eval_whs_section(section->postposition, whs_traverser);
	if (section->capture != NULL)
		eval_whs_section(section->capture, whs_traverser);
}

void eval_whs_literal(EXP* exp, Traverser* whs_traverser) {
	Pair* literal = pair_new(exp->str, KindLiteral);
	traverser_append(whs_traverser, literal);
}

void eval_whs_either(EXP* left, EXP* right, Traverser* whs_traverser) {
	Pair* either = pair_new(NULL, KindEither);
	traverser_append(whs_traverser, either);
	switch (left->kind)
	{
	case KindSection:
		eval_whs_section(left->section, whs_traverser);
		break;
	case KindEither:
		eval_whs_either(left->left, left->right, whs_traverser);
		break;
	}
	if (left->postposition != NULL)
		eval_whs_section(left->postposition, whs_traverser);
	Pair* end = pair_new(NULL, KindEnd);
	traverser_append(whs_traverser, end);
	switch (right->kind)
	{
	case KindSection:
		eval_whs_section(right->section, whs_traverser);
		break;
	case KindEither:
		eval_whs_either(right->left, right->right, whs_traverser);
		break;
	}
	if (right->postposition != NULL)
		eval_whs_section(right->postposition, whs_traverser);
	Pair* end2 = pair_new(NULL, KindEnd);
	traverser_append(whs_traverser, end2);
	Pair* end3 = pair_new(NULL, KindEnd);
	traverser_append(whs_traverser, end3);
}

void eval_whs_quantifier(EXP* exp, Traverser* whs_traverser) {
	Pair* quanitfier = pair_new(NULL, KindQuantifier);
	Pair* subscript = pair_new(exp->left->str, KindSubscript);
	Pair* superscript = pair_new(exp->right->str, KindSuperscript);
	traverser_append(whs_traverser, quanitfier);
	traverser_append(whs_traverser, subscript);
	traverser_append(whs_traverser, superscript);
}

void eval_whs_capture(EXP* exp, Traverser* whs_traverser) {
	Pair* capture = pair_new(NULL, KindCapture);
	Pair* capture_number = pair_new(exp->str, KindCaptureNumber);
	traverser_append(whs_traverser, capture);
	traverser_append(whs_traverser, capture_number);
}
