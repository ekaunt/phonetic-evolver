%{
#include <stdio.h>
#include <unicode/uchar.h>
#include "include/parse-ast.h"
EXP* root;
extern int yylineno;
extern char* yytext;
int yylex();
void yyerror(const char*);
%}
%locations
%define parse.error verbose

%start language
%token BECOMES SURROUND  WHEN EOL COLON COMMA X PLACEHOLDER
%token LEFTPAREN RIGHTPAREN LEFTBRACE RIGHTBRACE
%token<letters> LETTERS EMPTY CAPTURE CAPS SUBSCRIPT SUPERSCRIPT

%type<exp> lhs left_hand_side rhs right_hand_side whs when_hand_side whs_contents optional either comma_separated 
		statement postposition capture quantifier

%code requires {
	#include <unicode/uchar.h>
	#include "include/ast.h"
}
%union {
	char* letters;
	EXP* exp;
}

%%

language: statement			 					{ root = $1; 								}
		;
statement
		: lhs BECOMES rhs WHEN whs 				{ $$ = EXP_make_statement_when($1, $3, $5);}
		| lhs BECOMES rhs 						{ $$ = EXP_make_statement($1, $3); 			}
		;
lhs 	: left_hand_side postposition capture		{ $$ = EXP_make_section($1, $2, $3); 	}
		| lhs left_hand_side postposition capture	{ $$ = EXP_make_binary($1, $2, $3, $4);	}
		;
left_hand_side
		: EMPTY									{ $$ = EXP_make_literal(""); 				}
		| X										{ $$ = EXP_make_any(); 						}
		| LETTERS 								{ $$ = EXP_make_literal($1); 				}
		| optional 								{ $$ = EXP_make_section($1, NULL, NULL); 	}
		| either 								{ $$ = EXP_make_section($1, NULL, NULL); 	}
		;
rhs 	: rhs right_hand_side 					{ $$ = EXP_make_binary($1, $2, NULL, NULL);	}
		| right_hand_side 						{ $$ = EXP_make_section($1, NULL, NULL); 	}
		;
right_hand_side
		: EMPTY									{ $$ = EXP_make_literal("");				}
		| CAPTURE								{ $$ = EXP_make_capture($1);				}
		| LETTERS 								{ $$ = EXP_make_literal($1); 				}
		;
whs 	: when_hand_side PLACEHOLDER when_hand_side	{ $$ = EXP_make_whs($1, $3);			}
	 	| PLACEHOLDER when_hand_side			{ $$ = EXP_make_whs(NULL, $2);				}
	 	| when_hand_side PLACEHOLDER			{ $$ = EXP_make_whs($1, NULL);				}
		;
when_hand_side
		: when_hand_side whs_contents postposition	{ $$ = EXP_make_binary($1, $2, $3, NULL);}
		| whs_contents postposition				{ $$ = EXP_make_section($1, $2, NULL);		}
		;
whs_contents
		: LETTERS								{ $$ = EXP_make_literal($1);				}
		| X										{ $$ = EXP_make_any();						}
		| optional 								{ $$ = EXP_make_section($1, NULL, NULL); 	}
		| either 								{ $$ = EXP_make_section($1, NULL, NULL); 	}
		;
postposition
		: %empty								{ $$ = NULL;								}
		| quantifier							{ $$ = EXP_make_section($1, NULL, NULL);	}
		;
quantifier
		: SUBSCRIPT								{ $$ = EXP_make_quantifier($1, NULL);		}
		| SUBSCRIPT SUPERSCRIPT					{ $$ = EXP_make_quantifier($1, $2);			}
		;
capture
		: %empty								{ $$ = NULL;								}
		| CAPTURE								{ $$ = EXP_make_capture($1);				}
		;
optional: LEFTPAREN lhs RIGHTPAREN	 			{ $$ = EXP_make_optional($2); 				}
		;
either 	: LEFTBRACE comma_separated RIGHTBRACE	{ $$ = EXP_make_section($2, NULL, NULL);	}
		;
comma_separated
		: comma_separated COMMA lhs				{ $$ = EXP_make_either($1, $3); 			}
		| lhs 									{ $$ = EXP_make_section($1, NULL, NULL);	}
		;

%%

void yyerror(const char* msg) {
	fprintf(stderr, "Line %d: %s at '%s'\n", yylineno, msg, yytext);
}