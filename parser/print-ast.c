#include <stdlib.h>
#include "include/print-ast.h"
#include "include/ast.h"

void print_indents(int indent) {
	// u_printf("%i", indent);
	for (int i = 0; i < indent; ++i) {
		u_printf("| ");
	}
}

void print_indented(EXP* e, int indent) {
	print_indents(indent++);
	switch (e->kind) {
	case KindSection:
		u_printf("section\n");
		print_indented(e->section, indent);
		break;

	case KindBinary:
		u_printf("binary\n");
		print_indented(e->left, indent);
		print_indented(e->right, indent);
		break;

	case KindLiteral:
		u_printf("literal: %S\n", e->str);
		break;

	case KindOptional:
		u_printf("optional\n");
		print_indented(e->section, indent);
		break;

	case KindEither:
		u_printf("either\n");
		print_indented(e->left, indent);
		print_indented(e->right, indent);
		break;

	case KindStatement:
		u_printf("lhs\n");
		print_indented(e->left, indent);
		u_printf("rhs\n");
		print_indented(e->right, indent);
		break;

	case KindStatementWhen:
		u_printf("lhs\n");
		print_indented(e->left, indent);
		u_printf("rhs\n");
		print_indented(e->right, indent);
		u_printf("whs\n");
		print_indented(e->postposition, indent);
		return;
	
	case KindWHS:
		if (e->left) {
			u_printf("when preceded by\n");
			print_indented(e->left, indent);
		}
		if (e->right) {
			u_printf("when followed by\n");
			print_indented(e->right, indent);
		}
		break;

	case KindAny:
		u_printf("Any\n");
		break;
	
	case KindQuantifier:
		u_printf("Quantifier\n");
		print_indented(e->left, indent);
		if (e->right->str[0] != '\0')
			print_indented(e->right, indent);
		break;

	case KindSubscript:
		u_printf("subscript: %S\n", e->str);
		break;

	case KindSuperscript:
		u_printf("Superscript: %S\n", e->str);
		break;
	case KindCapture:
		u_printf("Capture: %S\n", e->str);
		break;
	}

	if (e->postposition != NULL) {
		print_indents(indent);u_printf("postposition:\n");
		print_indented(e->postposition, indent);
	}
	if (e->capture != NULL)
		print_indented(e->capture, indent);
}

void printEXP(EXP* e) {
	int indent = 0;
	print_indented(e, indent);
}