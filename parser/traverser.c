#include "include/traverser.h"

void traverser_new(Traverser** traverser) {
	*traverser = NEW(Traverser);
	(*traverser)->count = 0;
	(*traverser)->head = NULL;
	(*traverser)->tail = NULL;
}

void traverser_destroy(Traverser** traverser) {
	Node* temp;
	for (Node* head = (*traverser)->head; head != NULL; head = temp) {
		temp = head->next;
		if (head->pair != NULL) {
			free(head->pair);
		}
		free(head);
	}
	free(*traverser);
}

uint8_t traverser_is_empty(Traverser* traverser) {
	return traverser->head == NULL || traverser->tail == NULL;
}

void traverser_restart(Traverser* traverser, Node* from) {
	if (from != NULL)
		traverser->current = from;
	else
		traverser->current = NULL;
}

void traverser_append(Traverser* traverser, Pair* pair) {
	Node* tmp = NEW(Node);
	tmp->pair = pair;
	tmp->next = NULL;
	if (traverser_is_empty(traverser)) {
		traverser->head = tmp;
		traverser->tail = tmp;
	} else {
		traverser->tail->next = tmp;
		traverser->tail = tmp;
	}
	++traverser->count;
}

Node* traverser_next_node(Traverser* traverser) {
	if (traverser->current == NULL) {
		traverser->current = traverser->head;
	} else {
		traverser->current = traverser->current->next;
	}
	// u_printf("current traverser: %i\n", traverser->current->pair->kind);
	return traverser->current;
}

Pair* traverser_next(Traverser* traverser) {
	if (traverser->current == NULL) {
		traverser->current = traverser->head;
	} else {
		traverser->current = traverser->current->next;
	}
	// u_printf("current traverser: %i\n", traverser->current->pair->kind);
	return traverser->current->pair;
}

Pair* traverser_peek(Traverser* traverser) {
	if (traverser->current == NULL) {
		return traverser->head->pair;
	} else if (traverser->current->pair->kind == KindEof) {
		return traverser->current->pair;
	} else {
		return traverser->current->next->pair;
	}
}

/* - - - - - - - - - - - - PAIR - - - - - - - - - - - - - */

Pair* pair_new(UChar* str, ExpressionKind kind) {
	Pair* pair = NEW(Pair);
	pair->str = str;
	pair->kind = kind;
	return pair;
}