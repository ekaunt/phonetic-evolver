#include "include/captures.h"

UChar captures[CAPTURES_SIZE][CAPTURES_MAX_SIZE];

void capture_set(uint8_t pos, UChar capture[]) {
	if (pos > 0 && pos < CAPTURES_SIZE) {
		u_strncpy(captures[pos], capture, CAPTURES_MAX_SIZE);
		// u_printf("capture[%i]: %S\n", pos, captures[pos]);
	}
}

UChar* capture_get(uint8_t pos) {
	// u_printf("get capture[%i]: %S\n", pos, captures[pos]);
	return captures[pos];
}